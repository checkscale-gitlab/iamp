# IAMP

(Apache - MySQL - PHP7.1)
----
A simple and fast deploying environment solution for education, development and production using virtual machine 
An idea from vagrant and WSL.

------------------------------------------------------------------------

### About Project

As a php developer, I had struggled to build a fast and easy environment using technologies such as Docker, WSL and Vagrant, but when the time came to execute a command in <code>php-cli</code> they all fail in Windows platform, due to the structure of NTFS.
So, I decided to create a cross-platform solution for this issue.

I built a virtual machine which carries sets of services like webserver, database and programing languages.
The only problem was, how can I share my code between the VM and Host OS.

The first approach using an option that VMware provides, a 'shared folder' between Guest and Host, But this could only work properly on Linux and MacOS, meanwhile in Windows running time reaches up to x20.

The second option using a SFTP protocol which was quite good, but in the term of IDEs some of them like Jetbrains PhpStorm became laggy and useless

The third option using parallel project, one in the VM and the other on host OS, syncing them using SVN or SFTP
This option requires hard disk space double size of your project.


##### The solution

Using a samba shared folder could actually fulfill this need, but only under a condition which your user must be <code>root</code>,
WSL uses this technology but WSL itself is quite bit downfall due to the lack of optimization (Most of these issues fixed in WSL 2)

------------------------------------------------------------------------

### How to setup an environment

------------------------------------------------------------------------

#### Step 1

##### Install an IDE <br>
##### Install 7Zip <br>
- [Download](https://www.7-zip.org/download.html)

##### Install VMware
- ###### Mac
    - [Fusion](https://my.vmware.com/en/web/vmware/info/slug/desktop_end_user_computing/vmware_fusion/11_0#product_downloads)

- ###### Windows
    - [Player](https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html)

    - [Workstation](https://www.vmware.com/content/vmware/vmware-published-sites/us/products/workstation-pro/workstation-pro-evaluation.html.html)

- ###### Linux
    - [Player](https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html)

    - [Workstation](https://www.vmware.com/content/vmware/vmware-published-sites/us/products/workstation-pro/workstation-pro-evaluation.html.html)

#### Step 2

##### Download VM image (1.2GB)

- Faster for America and EU countries <br>
    <code>http://igostudio.net/iamp/iamp-vm.7z</code>
- Faster for MENA and Asia Pacific <br>
    <code>http://srv.igostudio.net/iamp/iamp-vm.7z</code>

##### Verify your file using [CHECKSUM](https://gitlab.com/igln/iamp/-/blob/master/CHECKSUM)
- MD5
    <code>4373a11134bfddbc9d3c73184dd1af9a</code>
- SHA1
    <code>e1f1f934aee3f3794affcbcc5733732e21eef386</code>

#### Step 3

Extract files using 7Zip
And Open <code>.vmx</code> using vmware
Change the specs as you like
Start!

#### Step 4

##### Credentials
- OS
    - username<br>
        <code>root</code>
    - password <br>
        <code>1</code>
- Samba
    - username<br>
        <code>root</code>
    - password <br>
        <code>1</code>

Connect to Samba using you vm ip
<br>
<code>\\\192.168.x.x\www\

for exp:
<code>\\\192.168.44.3\www\

And the <code>/var/www/</code> directory in the vm will became available inside your host os.

#### Step 5

Happy Coding :D


---------

### Debug
##### Enable you debug for development, Follow this [instruction](https://gitlab.com/igln/iamp/-/blob/master/DEBUG.md)

---------

If you had any question please submit an issue or email me at:<br>
[info@iGolchin.com](mailto:info@igolchin.com)
