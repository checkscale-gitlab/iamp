# Enabling PHP Debug

###### Note: This instruction is only for Jetbrains PhpStorm (Atom and Netbeans coming soon)

----

#### Jetbrains PhpStorm

#### Step 1
###### Install [PhpStorm](https://www.jetbrains.com/phpstorm/download/)
###### Install Xdebug Helper
- ###### [Chrome](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc?hl=en)

- ###### [Firefox](https://addons.mozilla.org/en-US/firefox/addon/xdebug-helper-for-firefox/)

#### Step 2
###### Find you VM ip address (Enter <code>ifconfig</code> in VM)
###### Open Project in PhpStorm

#### Step 3

###### Open PhpStorm setting and navigate to:
<br>
<code>Languages & Frameworks -> PHP</code>
<br>
<br>

- ###### Debug
  - ###### Debug Port to <code>9000</code>
- Servers
  - ###### Add a new server and name it something
  - ###### Enter the domain name or you VM ip inside host
  - ###### Enter the port number of your apachce, Default <code>80</code>
  - ###### Select Use Path mappings
    - ###### Select your project root
    - ###### Enter absolute address inside your vm (exp <code>/var/www/html/</code>)
    - ###### Hit OK!
  
#### Step 4
- ###### Turn on debug listener inside PhpStorm
- ###### Turn on Xdebug Helper inside your browser

#### Step 5
###### Refresh and Debug
